package com.tokodistributor.jetpack_submission1.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class ImageLoader : AppGlideModule()