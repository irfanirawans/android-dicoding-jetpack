package com.tokodistributor.jetpack_submission1.view.playlist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.tokodistributor.jetpack_submission1.R
import com.tokodistributor.jetpack_submission1.data.model.PlaylistResponse
import com.tokodistributor.jetpack_submission1.util.GlideApp

class PlaylistAdapter(
    private val data: ArrayList<PlaylistResponse>,
    private val playlistItemClickListener: PlaylistItemClickListener
) :
    RecyclerView.Adapter<PlaylistAdapter.ItemHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.playlist_item,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bindItem(
            item = data[position],
            playlistItemClickListener = playlistItemClickListener
        )
    }

    class ItemHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(item: PlaylistResponse, playlistItemClickListener: PlaylistItemClickListener) {
            view.apply {
                findViewById<RelativeLayout>(R.id.rel_playlist_container).setOnClickListener {
                    playlistItemClickListener.onPlaylistItemClicked(item = item)
                }
                findViewById<TextView>(R.id.txt_playlist_title).text = item.original_title
                findViewById<TextView>(R.id.txt_playlist_date).text = item.release_data
                findViewById<TextView>(R.id.txt_playlist_rating).text = item.vote.toString()
                findViewById<TextView>(R.id.txt_playlist_vote).text = item.popularity.toString()
                GlideApp
                    .with(view)
                    .load(item.poster_path)
                    .into(findViewById(R.id.img_playlist_cover))
            }
        }
    }
}
