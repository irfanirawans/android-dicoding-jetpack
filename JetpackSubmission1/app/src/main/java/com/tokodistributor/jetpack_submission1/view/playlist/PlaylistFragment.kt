package com.tokodistributor.jetpack_submission1.view.playlist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.tokodistributor.android.util.ext.navigator
import com.tokodistributor.android.util.ext.putArgs
import com.tokodistributor.jetpack_submission1.R
import com.tokodistributor.jetpack_submission1.data.PlaylistDataGenerator
import com.tokodistributor.jetpack_submission1.data.model.PlaylistResponse
import com.tokodistributor.jetpack_submission1.view.main.MainVM
import com.tokodistributor.jetpack_submission1.view.playlistdetail.PlaylistDetailActivity
import kotlinx.android.synthetic.main.playlist_fragment.*

class PlaylistFragment : Fragment(), PlaylistItemClickListener {

    private lateinit var viewModel: MainVM

    override fun onPlaylistItemClicked(item: PlaylistResponse) {
        navigator<PlaylistDetailActivity> {
            putExtra(PlaylistDetailActivity.POSTER_PATH, item.poster_path)
            putExtra(PlaylistDetailActivity.POPULARITY, item.popularity)
            putExtra(PlaylistDetailActivity.RATING, item.vote)
            putExtra(PlaylistDetailActivity.RELEASE_DATE, item.release_data)
            putExtra(PlaylistDetailActivity.TITLE, item.original_title)
            putExtra(PlaylistDetailActivity.OVERVIEW, item.overview)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.playlist_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(MainVM::class.java)
        setupPlaylist()
    }

    private fun setupPlaylist() {
        val playlistType = arguments?.getInt(PLAYLIST_TYPE)
        if (playlistType != null) {
            val data = if (playlistType == MOVIES) {
                viewModel.getMoviesList()
            } else {
                viewModel.getTvShowsList()
            }

            recycler_playlist.apply {
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
                adapter =
                    PlaylistAdapter(data = data, playlistItemClickListener = this@PlaylistFragment)
            }
        }
    }

    companion object {
        private const val PLAYLIST_TYPE = "PLAYLIST_TYPE"
        const val MOVIES = 0
        const val TV_SHOWS = 1

        fun newInstance(playlistType: Int) = PlaylistFragment().putArgs {
            putInt(PLAYLIST_TYPE, playlistType)
        }
    }
}