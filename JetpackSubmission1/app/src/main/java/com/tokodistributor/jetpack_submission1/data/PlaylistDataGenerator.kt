package com.tokodistributor.jetpack_submission1.data

import com.tokodistributor.jetpack_submission1.R
import com.tokodistributor.jetpack_submission1.data.model.PlaylistResponse

object PlaylistDataGenerator {
    val movieImageList = listOf(
        R.drawable.poster_a_start_is_born,
        R.drawable.poster_alita,
        R.drawable.poster_bohemian,
        R.drawable.poster_crimes,
        R.drawable.poster_how_to_train,
        R.drawable.poster_infinity_war,
        R.drawable.poster_marry_queen,
        R.drawable.poster_ralph,
        R.drawable.poster_spiderman,
        R.drawable.poster_t34
    )

    val tvShowImageList = listOf(
        R.drawable.poster_the_simpson,
        R.drawable.poster_arrow,
        R.drawable.poster_ncis,
        R.drawable.poster_the_walking_dead,
        R.drawable.poster_naruto_shipudden,
        R.drawable.poster_doom_patrol,
        R.drawable.poster_flash,
        R.drawable.poster_gotham,
        R.drawable.poster_god,
        R.drawable.poster_hanna
    )

    fun getMoviesList() = ArrayList<PlaylistResponse>().apply {
        add(
            PlaylistResponse(
                original_title = "A Start is Born",
                popularity = 160.624,
                release_data = "1989-12-17",
                vote = 7.1,
                overview = "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
                poster_path = movieImageList[0]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Alita: Battle Angel",
                popularity = 51.786,
                release_data = "2019-02-14",
                vote = 6.8,
                overview = "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.",
                poster_path = movieImageList[1]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Bohemian Rhapsody",
                popularity = 35.479,
                release_data = "2018-11-02",
                vote = 8.1,
                overview = "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.",
                poster_path = movieImageList[2]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Fantastic Beasts: The Crimes of Grindelwald",
                popularity = 35.697,
                release_data = "2018-11-16",
                vote = 6.9,
                overview = "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.",
                poster_path = movieImageList[3]
            )
        )
        add(
            PlaylistResponse(
                original_title = "How to Train Your Dragon: The Hidden World",
                popularity = 100.256,
                release_data = "2007-02-15",
                vote = 2.5,
                overview = "As Hiccup fulfills his dream of creating a peaceful dragon utopia, Toothless’ discovery of an untamed, elusive mate draws the Night Fury away. When danger mounts at home and Hiccup’s reign as village chief is tested, both dragon and rider must make impossible decisions to save their kind.",
                poster_path = movieImageList[4]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Avengers: Infinity War",
                popularity = 68.637,
                release_data = "2018-04-27",
                vote = 8.3,
                overview = "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.",
                poster_path = tvShowImageList[5]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Mary Queen of Scots",
                popularity = 27.014,
                release_data = "2018-12-21",
                vote = 6.3,
                overview = "Mary Stuart's attempt to overthrow her cousin Elizabeth I, Queen of England, finds her condemned to years of imprisonment before facing execution. ",
                poster_path = movieImageList[6]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Ralph Breaks the Internet",
                popularity = 100.092,
                release_data = "2018-11-21",
                vote = 7.1,
                overview = "Six years after the events of \"Wreck-It Ralph,\" Ralph and Vanellope, now friends, discover a wi-fi router in their arcade, leading them into a new adventure.",
                poster_path = movieImageList[7]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Spider-Man: Into the Spider-Verse",
                popularity = 255.606,
                release_data = "2018-12-14",
                vote = 8.4,
                overview = "Teen Miles Morales becomes Spider-Man of his reality, crossing his path with five counterparts from other dimensions to stop a threat for all realities.",
                poster_path = movieImageList[8]
            )
        )
        add(
            PlaylistResponse(
                original_title = "T-34",
                popularity = 4.114,
                release_data = "2018-12-27",
                vote = 6.4,
                overview = "In 1944, a courageous group of Russian soldiers managed to escape from German captivity in a half-destroyed legendary T-34 tank. Those were the times of unforgettable bravery, fierce fighting, unbreakable love, and legendary miracles.",
                poster_path = movieImageList[9]
            )
        )
    }

    fun getTvShowsList() = ArrayList<PlaylistResponse>().apply {
        add(
            PlaylistResponse(
                original_title = "The Simpsons",
                popularity = 160.624,
                release_data = "1989-12-17",
                vote = 7.1,
                overview = "Set in Springfield, the average American town, the show focuses on the antics and everyday adventures of the Simpson family; Homer, Marge, Bart, Lisa and Maggie, as well as a virtual cast of thousands. Since the beginning, the series has been a pop culture icon, attracting hundreds of celebrities to guest star. The show has also made name for itself in its fearless satirical take on politics, media and American life in general.",
                poster_path = tvShowImageList[0]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Arrow",
                popularity = 200.588,
                release_data = "2012-10-10",
                vote = 5.8,
                overview = "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.",
                poster_path = tvShowImageList[1]
            )
        )
        add(
            PlaylistResponse(
                original_title = "NCIS",
                popularity = 70.767,
                release_data = "2003-09-23",
                vote = 6.7,
                overview = "From murder and espionage to terrorism and stolen submarines, a team of special agents investigates any crime that has a shred of evidence connected to Navy and Marine Corps personnel, regardless of rank or position.",
                poster_path = tvShowImageList[2]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Fear the Walking Dead",
                popularity = 245.087,
                release_data = "2015-08-23",
                vote = 6.3,
                overview = "What did the world look like as it was transforming into the horrifying apocalypse depicted in \\\"The Walking Dead\\\"? This spin-off set in Los Angeles, following new characters as they face the beginning of the end of the world, will answer that question.",
                poster_path = tvShowImageList[3]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Naruto Shippūden",
                popularity = 100.256,
                release_data = "2007-02-15",
                vote = 2.5,
                overview = "Naruto Shippuuden is the continuation of the original animated TV series Naruto.The story revolves around an older and slightly more matured Uzumaki Naruto and his quest to save his friend Uchiha Sasuke from the grips of the snake-like Shinobi, Orochimaru. After 2 and a half years Naruto finally returns to his village of Konoha, and sets about putting his ambitions to work, though it will not be easy, as He has amassed a few (more dangerous) enemies, in the likes of the shinobi organization; Akatsuki.",
                poster_path = tvShowImageList[4]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Doom Patrol",
                popularity = 84.469,
                release_data = "2019-02-15",
                vote = 6.4,
                overview = "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.",
                poster_path = tvShowImageList[5]
            )
        )
        add(
            PlaylistResponse(
                original_title = "The Flash",
                popularity = 206.084,
                release_data = "2014-10-07",
                vote = 6.7,
                overview = "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \\\"meta-human\\\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
                poster_path = tvShowImageList[6]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Gotham",
                popularity = 125.102,
                release_data = "2014-09-22",
                vote = 6.9,
                overview = "Before there was Batman, there was GOTHAM. \\n\\nEveryone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker? ",
                poster_path = tvShowImageList[7]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Game of Thrones",
                popularity = 105.039,
                release_data = "2011-04-17",
                vote = 8.1,
                overview = "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.",
                poster_path = tvShowImageList[8]
            )
        )
        add(
            PlaylistResponse(
                original_title = "Hanna",
                popularity = 0.0,
                release_data = "1234-12-34",
                vote = 0.0,
                overview = "This thriller and coming-of-age drama follows the journey of an extraordinary young girl as she evades the relentless pursuit of an off-book CIA agent and tries to unearth the truth behind who she is. Based on the 2011 Joe Wright film.",
                poster_path = tvShowImageList[9]
            )
        )
    }

}