package com.tokodistributor.android.util.ext

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.tokodistributor.jetpack_submission1.util.ViewModelFactory

inline fun <FRAGMENT : Fragment> FRAGMENT.putArgs(argsBuilder: Bundle.() -> Unit):
        FRAGMENT = this.apply { arguments = Bundle().apply(argsBuilder) }

/**
 * Using it for move to another page with a single param
 * If you want sand data using object, model, etc convert it into string using Gson
 *
 * @param param => param with string type
 */
inline fun <reified T : AppCompatActivity> Fragment.navigator(
    intentParams: Intent.() -> Unit
) {
    val intent = Intent(requireContext(), T::class.java)
    intent.intentParams()
    startActivity(intent)

    //=========== How to using it ===========
    // navigator<DetailActivity> {
    //        putExtra("KEY1" , "VALUE1")
    //        putExtra("KEY2" , "VALUE2")
    //    }
    //=======================================
}
