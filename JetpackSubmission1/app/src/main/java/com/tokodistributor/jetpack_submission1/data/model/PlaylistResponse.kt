package com.tokodistributor.jetpack_submission1.data.model

data class PlaylistResponse(
    var original_title: String,
    var popularity: Double,
    var release_data: String,
    var vote: Double,
    var overview: String,
    var poster_path: Int
)