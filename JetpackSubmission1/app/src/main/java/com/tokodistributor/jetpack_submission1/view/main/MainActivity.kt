package com.tokodistributor.jetpack_submission1.view.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.tokodistributor.jetpack_submission1.R
import com.tokodistributor.jetpack_submission1.view.playlist.PlaylistFragment
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        setupPagerPlaylist()
        setupTabPlaylist()
    }

    private fun setupTabPlaylist() {
        tab_main_playlist.setupWithViewPager(pager_main_playlist)
    }

    private fun setupPagerPlaylist() {
        val mainPlaylistPagerAdapter =
            MainPlaylistPagerAdapter(fragmentManager = supportFragmentManager).apply {
                addPlaylistFragment(
                    fragment = PlaylistFragment.newInstance(playlistType = PlaylistFragment.MOVIES),
                    title = "MOVIES"
                )
                addPlaylistFragment(
                    fragment = PlaylistFragment.newInstance(playlistType = PlaylistFragment.TV_SHOWS),
                    title = "TV SHOWS"
                )
            }

        pager_main_playlist.apply {
            adapter = mainPlaylistPagerAdapter
            offscreenPageLimit = 2
        }
    }
}
