package com.tokodistributor.jetpack_submission1.view.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class MainPlaylistPagerAdapter(fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {

    private var fragmentsList = ArrayList<Fragment>()
    private val titlesList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return fragmentsList[position]
    }

    override fun getCount() = fragmentsList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return titlesList[position]
    }

    fun addPlaylistFragment(fragment: Fragment, title: String) {
        fragmentsList.add(fragment)
        titlesList.add(title)
    }
}