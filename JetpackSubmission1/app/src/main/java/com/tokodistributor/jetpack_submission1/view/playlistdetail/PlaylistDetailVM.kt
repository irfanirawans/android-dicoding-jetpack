package com.tokodistributor.jetpack_submission1.view.playlistdetail

import androidx.lifecycle.ViewModel

class PlaylistDetailVM(

) : ViewModel() {

    private var title = ""
    private var posterPath = 0
    private var releaseDate = ""
    private var rating = 0.0
    private var popularity = 0.0
    private var overview = ""

    fun setupPlaylistDetailContent(
        title: String, posterPath: Int, releaseDate: String, rating: Double,
        popularity: Double, overview: String
    ) {
        this.title = title
        this.posterPath = posterPath
        this.releaseDate = releaseDate
        this.rating = rating
        this.popularity = popularity
        this.overview = overview
    }

    fun getTitle() = title

    fun getPosterPath() = posterPath

    fun getReleaseDate() = releaseDate

    fun getRating() = rating

    fun getPopularity() = popularity

    fun getOverview() = overview

}