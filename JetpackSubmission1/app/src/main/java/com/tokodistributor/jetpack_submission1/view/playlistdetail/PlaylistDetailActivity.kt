package com.tokodistributor.jetpack_submission1.view.playlistdetail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.tokodistributor.jetpack_submission1.R
import com.tokodistributor.jetpack_submission1.util.GlideApp
import com.tokodistributor.jetpack_submission1.util.getViewModel
import kotlinx.android.synthetic.main.playlist_detail_activity.*

class PlaylistDetailActivity : AppCompatActivity() {

    lateinit var viewModel: PlaylistDetailVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.playlist_detail_activity)

        setupPlaylistDetailToolbar()
        setupPlaylistDetailVM()

        val bundle = intent.extras
        if (bundle != null) {
            val title = bundle.getString(TITLE) ?: ""
            val posterPath = bundle.getInt(POSTER_PATH, 0)
            val releaseDate = bundle.getString(RELEASE_DATE) ?: ""
            val rating = bundle.getDouble(RATING, 0.0)
            val popularity = bundle.getDouble(POPULARITY, 0.0)
            val overview = bundle.getString(OVERVIEW) ?: ""

            viewModel.setupPlaylistDetailContent(
                title = title,
                posterPath = posterPath,
                releaseDate = releaseDate,
                rating = rating,
                popularity = popularity,
                overview = overview
            )

            setupPlaylistDetail(
                title = viewModel.getTitle(),
                posterPath = viewModel.getPosterPath(),
                releaseDate = viewModel.getReleaseDate(),
                rating = viewModel.getRating(),
                popularity = viewModel.getPopularity(),
                overview = viewModel.getOverview()
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun setupPlaylistDetailVM(
    ) {
        viewModel = getViewModel { PlaylistDetailVM() }
    }

    private fun setupPlaylistDetailToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowTitleEnabled(false)
        }
    }

    private fun setupPlaylistDetail(
        title: String,
        posterPath: Int,
        releaseDate: String,
        rating: Double,
        popularity: Double,
        overview: String
    ) {
        txt_playlistDetail_title.text = title
        GlideApp.with(this)
            .load(posterPath)
            .placeholder(R.color.greyUltraLight)
            .error(R.color.greyUltraLight)
            .into(img_playlistDetail_cover)
        txt_playlistDetail_releaseDate.text = releaseDate
        txt_playlistDetail_rating.text = rating.toString()
        txt_playlistDetail_popularity.text = popularity.toString()
        txt_playlistDetail_overview.text = overview
    }

    companion object {
        const val POSTER_PATH = "POSTER_PATH"
        const val TITLE = "TITLE"
        const val RELEASE_DATE = "RELEASE_DATE"
        const val RATING = "RATING"
        const val POPULARITY = "POPULARITY"
        const val OVERVIEW = "OVERVIEW"
    }
}
