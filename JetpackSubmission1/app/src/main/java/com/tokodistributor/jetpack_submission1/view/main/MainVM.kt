package com.tokodistributor.jetpack_submission1.view.main

import androidx.lifecycle.ViewModel
import com.tokodistributor.jetpack_submission1.data.PlaylistDataGenerator

class MainVM : ViewModel() {

    fun getTvShowsList() = PlaylistDataGenerator.getTvShowsList()

    fun getMoviesList() = PlaylistDataGenerator.getMoviesList()

}