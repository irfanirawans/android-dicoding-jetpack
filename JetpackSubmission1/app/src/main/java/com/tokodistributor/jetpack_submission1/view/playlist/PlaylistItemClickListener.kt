package com.tokodistributor.jetpack_submission1.view.playlist

import com.tokodistributor.jetpack_submission1.data.model.PlaylistResponse

interface PlaylistItemClickListener {
    fun onPlaylistItemClicked(item: PlaylistResponse)
}