package com.tokodistributor.jetpack_submission1

import com.tokodistributor.jetpack_submission1.data.PlaylistDataGenerator
import com.tokodistributor.jetpack_submission1.view.main.MainVM
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.After
import org.junit.Before
import org.junit.Test

class MainVMTest {

    private lateinit var viewModel: MainVM

    @Before
    fun setup() {
        viewModel = MainVM()
    }

    @Test
    fun `check if tv shows list  not null`() {
        val tvShowsList = PlaylistDataGenerator.getTvShowsList()
        assertNotNull(tvShowsList)
        assertEquals(10, tvShowsList.size)
    }

    @Test
    fun `check if movies list not null`() {
        val moviesList = PlaylistDataGenerator.getMoviesList()
        assertNotNull(moviesList)
        assertEquals(10, moviesList.size)
    }

}