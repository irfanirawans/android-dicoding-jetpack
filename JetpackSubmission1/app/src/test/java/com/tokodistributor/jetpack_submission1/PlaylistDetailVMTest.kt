package com.tokodistributor.jetpack_submission1

import com.tokodistributor.jetpack_submission1.data.PlaylistDataGenerator.movieImageList
import com.tokodistributor.jetpack_submission1.data.PlaylistDataGenerator.tvShowImageList
import com.tokodistributor.jetpack_submission1.data.model.PlaylistResponse
import com.tokodistributor.jetpack_submission1.view.playlistdetail.PlaylistDetailVM
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test

class PlaylistDetailVMTest {

    private lateinit var tvShowsVM: PlaylistDetailVM
    private lateinit var moviesVM: PlaylistDetailVM
    private lateinit var dummyTvShowPlaylist: PlaylistResponse
    private lateinit var dummyMoviesPlaylist: PlaylistResponse

    @Before
    fun setup() {
        tvShowsVM = PlaylistDetailVM()
        moviesVM = PlaylistDetailVM()

        dummyTvShowPlaylist = PlaylistResponse(
            original_title = "The Simpsons",
            popularity = 160.624,
            release_data = "1989-12-17",
            vote = 7.1,
            overview = "Set in Springfield, the average American town, the show focuses on the antics and everyday adventures of the Simpson family; Homer, Marge, Bart, Lisa and Maggie, as well as a virtual cast of thousands. Since the beginning, the series has been a pop culture icon, attracting hundreds of celebrities to guest star. The show has also made name for itself in its fearless satirical take on politics, media and American life in general.",
            poster_path = tvShowImageList[0]
        )

        dummyMoviesPlaylist = PlaylistResponse(
            original_title = "A Start is Born",
            popularity = 160.624,
            release_data = "1989-12-17",
            vote = 7.1,
            overview = "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.",
            poster_path = movieImageList[0]
        )

        tvShowsVM.setupPlaylistDetailContent(
            title = dummyTvShowPlaylist.original_title,
            posterPath = dummyTvShowPlaylist.poster_path,
            releaseDate = dummyTvShowPlaylist.release_data,
            rating = dummyTvShowPlaylist.vote,
            popularity = dummyTvShowPlaylist.popularity,
            overview = dummyTvShowPlaylist.overview
        )

        moviesVM.setupPlaylistDetailContent(
            title = dummyMoviesPlaylist.original_title,
            posterPath = dummyMoviesPlaylist.poster_path,
            releaseDate = dummyMoviesPlaylist.release_data,
            rating = dummyMoviesPlaylist.vote,
            popularity = dummyMoviesPlaylist.popularity,
            overview = dummyMoviesPlaylist.overview
        )
    }

    // Tv Shows List Test ==========================================================================

    @Test
    fun `check if tv show playlist title is not null`() {
        val title = tvShowsVM.getTitle()
        assertNotNull(title)
        assertEquals(title, dummyTvShowPlaylist.original_title)
    }

    @Test
    fun `check if tv show playlist popularity is not null`() {
        val popularity = tvShowsVM.getPopularity()
        assertNotNull(popularity)
        assertEquals(popularity, dummyTvShowPlaylist.popularity)
    }

    @Test
    fun `check if tv show playlist release date is not null`() {
        val releaseDate = tvShowsVM.getReleaseDate()
        assertNotNull(releaseDate)
        assertEquals(releaseDate, dummyTvShowPlaylist.release_data)
    }

    @Test
    fun `check if tv show playlist vote date is not null`() {
        val vote = tvShowsVM.getRating()
        assertNotNull(vote)
        assertEquals(vote, dummyTvShowPlaylist.vote)
    }

    @Test
    fun `check if tv show playlist overview is not null`() {
        val overview = tvShowsVM.getOverview()
        assertNotNull(overview)
        assertEquals(overview, dummyTvShowPlaylist.overview)
    }

    @Test
    fun `check if tv show playlist poster path is not null`() {
        val posterPath = tvShowsVM.getPosterPath()
        assertNotNull(posterPath)
        assertEquals(posterPath, dummyTvShowPlaylist.poster_path)
    }

    // Movies List Tes =============================================================================

    @Test
    fun `check if movie playlist title is not null`() {
        val title = moviesVM.getTitle()
        assertNotNull(title)
        assertEquals(title, dummyMoviesPlaylist.original_title)
    }

    @Test
    fun `check if movie playlist popularity is not null`() {
        val popularity = moviesVM.getPopularity()
        assertNotNull(popularity)
        assertEquals(popularity, dummyMoviesPlaylist.popularity)
    }

    @Test
    fun `check if movie playlist release date is not null`() {
        val releaseDate = moviesVM.getReleaseDate()
        assertNotNull(releaseDate)
        assertEquals(releaseDate, dummyMoviesPlaylist.release_data)
    }

    @Test
    fun `check if movie playlist vote date is not null`() {
        val vote = moviesVM.getRating()
        assertNotNull(vote)
        assertEquals(vote, dummyMoviesPlaylist.vote)
    }

    @Test
    fun `check if movie playlist overview is not null`() {
        val overview = moviesVM.getOverview()
        assertNotNull(overview)
        assertEquals(overview, dummyMoviesPlaylist.overview)
    }

    @Test
    fun `check if movie playlist poster path is not null`() {
        val posterPath = moviesVM.getPosterPath()
        assertNotNull(posterPath)
        assertEquals(posterPath, dummyMoviesPlaylist.poster_path)
    }
}